package com.zan.models;

import java.util.Comparator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Occasion implements Comparable<Occasion>{
	private String name;
	private String locationName;
	private int examinationTypeId;
	private String date;
	private String time;
	
	public Occasion() {
		super();
	}
	public Occasion(String date, String time) {
		super();
		this.date = date;
		this.time = time;
	}
	@Override
	public String toString() {
		return "Occasion [name=" + name + ", date=" + date + ", time=" + time + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public int getExaminationTypeId() {
		return examinationTypeId;
	}
	public void setExaminationTypeId(int examinationTypeId) {
		this.examinationTypeId = examinationTypeId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int compareTo(Occasion o) {
		if (this.date.compareTo(o.getDate()) != 0){
			return this.date.compareTo(o.getDate());
		}else {
			return this.time.compareTo(o.getTime());
		}
	}

}
