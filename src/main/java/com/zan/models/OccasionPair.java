package com.zan.models;

import java.util.Arrays;
import java.util.Comparator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OccasionPair implements Comparable<Occasion>{

	private Occasion[] occasions;
	
	@Override
	public String toString() {
		Arrays.sort(occasions);
		return "OccasionPair [occasions=" + Arrays.toString(occasions) + "]\n";
	}

	public Occasion[] getOccasions() {
		Arrays.sort(occasions);
		return occasions;
	}

	public void setOccasions(Occasion[] occasions) {
		this.occasions = occasions;
	}
//
//	public int compareTo(OccasionPair o) {
//		Arrays.sort(occasions);
//		if (Arrays.equals(occasions, o.getOccasions())) {
//			return 0;
//		}
//		Arrays.
//	}

	public int compareTo(Occasion o) {
		if ( this.getOccasions().length < 1)
		{
			throw new IllegalStateException();
		}
		return this.getOccasions()[0].compareTo(o);
	}

}
