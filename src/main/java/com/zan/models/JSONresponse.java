package com.zan.models;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JSONresponse {
	private OccasionPair[] data;
	private int status;
	@Override
	public String toString() {
		return "JSONresponse [data=" + Arrays.toString(data) + "]";
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public OccasionPair[] getData() {
		return data;
	}

	public void setData(OccasionPair[] data) {
		this.data = data;
	}
}
