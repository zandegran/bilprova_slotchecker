package com.zan.sloter;

import org.apache.http.client.methods.HttpPost;

import com.zan.models.Occasion;

public class Config {
	// Time between Checks
	public static final int SLEEPING_TIME = 5; // in Minutes
	// Range in YYYY-MM-DD and hh:mm
	public static final Occasion MIN = new Occasion("2019-05-15","09:00");
	public static final Occasion MAX = new Occasion("2019-05-25","09:00");
	
	// Request Details
	private static final String ID = "19880309-7214";
	//Location 1000134 Sollentuna, 1000140 Stockholm, 1000019 farsta, 1000326 jarfalla
	public static final String[][] LOCATION_IDS = {{"1000134", "Sollentuna"}, 
	                                               {"1000019", "farsta"}, 
	                                               {"1000326", "jarfla"}, 
	                                               {"1000141", "Jakobsberg"}}; 	
	private static final String LANGUAGE_ID = "4";			// 13 for Swedish 4 for english
	private static final String VEHICLE_ID = "2";			// 2 for Manual, 4 for Automatic, 1 for No Vehicle (i.e. Bringing you own car with 2 controls)
	// Get cookie from your request from browser
	private static final String COOKIE = "FpsExternalIdentity=5EC8EAB0A14759157B7ADBDB8AFC00AEA755AB26C67C562EAE4F93298FFAC93EA9ABD49E0D075F87A9D9E86E2B722E1E6E16BC9F06C5E891381D6BAF530A94F6F7C76EC00F2B7CCFF01C0545BE3AC2A875E9C60AE77A2F5C1B02E3CEDCDCEDC839D8058FECEFA79F5B43E99757C6456813DF47470D5BC86C5579F7C9AE86CD689A951A857D8B7A6740BF5AB9E6C46AC1115203CBA4B9630AC4865FE191E00BDEDD3628A648837DC9431EE2700460CECD";
	
	
	// Don't change
	public static final String URL = "https://fp.trafikverket.se/Boka/occasion-bundles";
	
	public static String getBody(int locationIdIndex) { // Input 
		String body = "{\"bookingSession\":{\"socialSecurityNumber\":\"" + ID + "\","   	// ID
				+ "\"licenceId\":5,"														// B
				+ "\"bookingModeId\":0,"
				+ "\"ignoreDebt\":false,"													// Interesting :O
				+ "\"examinationTypeId\":0},"
				+ "\"occasionBundleQuery\":{"
					+ "\"startDate\":\"" + MIN.getDate() + "T22:00:00.000Z\","				// Date after
					+ "\"locationId\":" + LOCATION_IDS[locationIdIndex][0] + ","												
					+ "\"languageId\":" + LANGUAGE_ID + ","
					+ "\"vehicleTypeId\":" + VEHICLE_ID + ","
					+ "\"tachographTypeId\":1,"
					+ "\"occasionChoiceId\":1,"
					+ "\"examinationTypeId\":0"
				+ "}}";
		return body;
	}
	public static void addHeaders(HttpPost httppost) {
		httppost.addHeader("Host", "fp.trafikverket.se");
		httppost.addHeader("Connection", "keep-alive");
		//httppost.addHeader("Content-Length", "313");
		httppost.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
		httppost.addHeader("Origin", "https://fp.trafikverket.se");
		httppost.addHeader("X-Requested-With", "XMLHttpRequest");
		httppost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
		httppost.addHeader("Content-Type", "application/json");
		httppost.addHeader("Referer", "https://fp.trafikverket.se/Boka/");
		httppost.addHeader("Accept-Encoding", "gzip, deflate, br");
		httppost.addHeader("Accept-Language", "sv,en-US;q=0.8,en;q=0.6,en-IN;q=0.4");
		//Please change this
		httppost.addHeader("Cookie", COOKIE);
	}
}
