package com.zan.sloter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.mp3transform.Decoder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zan.models.JSONresponse;
import com.zan.models.OccasionPair;

public class SlotChecker {
	private static final int SUCCESS = 200;
	private static Thread t1;
	
	public static void main (String[] args) throws Exception {		
		HttpClient httpclient = HttpClients.createDefault();
		
		doPeriodicChecking(httpclient);
	}
	
	private static void doPeriodicChecking(HttpClient httpclient) throws InterruptedException, ParseException, IOException {
		while (true) {
		  for (int i = 0 ; i < Config.LOCATION_IDS.length ; i++) {
		    HttpPost httppost = getConfiguredHttpPost(i);
  			HttpResponse response = null;
  			try {
  				//Execute and get the response.
  				response = httpclient.execute(httppost);
  			} 
  			catch (SocketException e){
  				System.out.println(getTime() + ": Network error");
  				Thread.sleep(60 * 1000);
  				continue;
  			}
  			catch (Exception e) {
  				e.printStackTrace();
  				Thread.sleep(60 * 1000);
  				continue;
  			}
  			HttpEntity entity = response.getEntity();
//  			System.out.println(entity == null? "No response": "Response Found");
  	    JSONresponse jsonResponse = null;
  			if (entity != null) {
  				String output = EntityUtils.toString(entity);
  				System.out.println(output);
  				ObjectMapper mapper = new ObjectMapper();
  		    	jsonResponse=mapper.readValue(output, JSONresponse.class);
  			    if (jsonResponse.getStatus() != SUCCESS) {
  			    	System.out.println(getTime() + ": Something went Wrong. Please check!");
  				    System.out.println("Output: " + output);
  			    	Thread.sleep(Config.SLEEPING_TIME * 1000 * 60);
  					continue;
  			    }
  			   
  			    checkForSlotsInRange(jsonResponse.getData(), Config.LOCATION_IDS[i][1]);			    
  			}
  			else {
  				// Something Broke: Play alarm
  			}
		  }
			Thread.sleep(Config.SLEEPING_TIME * 1000 * 60);
		}
	}
	
	private static HttpPost getConfiguredHttpPost(int locationIdIndex) throws UnsupportedEncodingException {
		HttpPost httppost = new HttpPost(Config.URL);	
		String body = Config.getBody(locationIdIndex);
		HttpEntity input = new ByteArrayEntity(body.getBytes("UTF-8"));
		httppost.setEntity(input);
		Config.addHeaders(httppost);
		return httppost;
	}
	
	private static String getTime() {
		DateFormat formatter = new SimpleDateFormat("dd,MMM - HH:mm:ss:SSS");
    	return formatter.format(System.currentTimeMillis());
	}
	private static void checkForSlotsInRange(OccasionPair[] slots, String location) throws IOException {
		List<OccasionPair> slotsInRange = getSlotsInRange(slots);
	    if (slotsInRange.size() > 0) {
	    	System.out.println(getTime() + ": Found "+ slotsInRange.size() +" in " + slots.length + " at " + location);
	    	System.out.println(Arrays.toString(slotsInRange.toArray()));
	    	// This script will call your phone via FaceTime. Script in ROOT/blob
	    	//Runtime.getRuntime().exec("osascript /Users/anselzandegran/Documents/FT.scpt");
	    	playAudio("src/main/resources/Punnagai.mp3");//getClass().getResource("")
	    }
	    else {
	    	System.out.println(getTime() + ": Not Found between " + Config.MIN.getDate() + " and " + Config.MAX.getDate() + " at \t" + location);
	    }
	}
	private static List<OccasionPair> getSlotsInRange (OccasionPair[] slots) {
		List<OccasionPair> slotsInRange = new ArrayList<OccasionPair>();
		for (OccasionPair slot: slots) {
			if (slot.compareTo(Config.MIN) > 0 && slot.compareTo(Config.MAX) < 0) {
				slotsInRange.add(slot);
			}
		}
		return slotsInRange;
	}
	
	private static void checkForNewSlots(OccasionPair[] SlotPairs, OccasionPair[] oldSlotPairs){
	    List<OccasionPair> newSlots = getNewSlots(SlotPairs, oldSlotPairs);			    
	    if(newSlots.size() > 0) {
	    	// Something changed
	    	System.out.println(getTime() + ": Something changed, New: " + newSlots.size());
	    } else {
	    	System.out.println(getTime() + ": Nothing changed, New: " + newSlots.size());
	    }
	    //System.out.println(Arrays.toString(newSlots.toArray()));
	}
	
	private static List<OccasionPair> getNewSlots (OccasionPair[] SlotPairs, OccasionPair[] oldSlotPairs) {
		List<OccasionPair> newSlots = new ArrayList<OccasionPair>();
		if (SlotPairs == null) {
			return newSlots;
		}
		for (OccasionPair SlotPair: SlotPairs) {
			boolean found = false;
			for (OccasionPair oldSlotPair: oldSlotPairs) {
				//System.out.println(" "+Arrays.equals(oldSlotPair.getOccasions(), SlotPair.getOccasions())+ "  \t" + Arrays.toString(oldSlotPair.getOccasions())+ "  \t" + Arrays.toString(SlotPair.getOccasions()));
				if (Arrays.equals(oldSlotPair.getOccasions(), SlotPair.getOccasions())){
					found = true;
					break;
				}
			}
			if (!found) {
				newSlots.add(SlotPair);
			}
		}
		//System.out.println("Total: " + newResponse.getData().length + " New: " + newSlots.size());
		return newSlots;
	}
	
	private static void playAudio(final String Filename) {
		if (t1 != null) {
			if(t1.isAlive()) {
				t1.interrupt();
			}
		}
		Thread t1 = new Thread(new Runnable() {
            public void run()
            {
                try{
                    Decoder decoder = new Decoder();
                        File file = new File(Filename);
                        FileInputStream in = new FileInputStream(file);
                        BufferedInputStream bin = new BufferedInputStream(in, 128 * 1024);
                        decoder.play(file.getName(), bin);
                        in.close();

                    decoder.stop();
                }
                catch(Exception exc){
                    exc.printStackTrace();
                    System.out.println("Failed to play the file.");
                }
            }});  
            t1.start();
	}

}
